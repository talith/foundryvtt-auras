function figorgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}
function hsl2rgb(h, s, l) {
    var r, g, b, m, c, x;
    if(!isFinite(h)) h = 0;
    if(!isFinite(s)) s = 0;
    if(!isFinite(l)) l = 0;
    h /= 60;
    if(h < 0) h = 6 - (-h % 6);
    h %= 6;
    s = Math.max(0, Math.min(1, s / 100));
    l = Math.max(0, Math.min(1, l / 100));
    c = (1 - Math.abs((2 * l) - 1)) * s;
    x = c * (1 - Math.abs((h % 2) - 1));
    if(h < 1) {
        r = c;
        g = x;
        b = 0;
    } else if(h < 2) {
        r = x;
        g = c;
        b = 0;
    } else if(h < 3) {
        r = 0;
        g = c;
        b = x;
    } else if(h < 4) {
        r = 0;
        g = x;
        b = c;
    } else if(h < 5) {
        r = x;
        g = 0;
        b = c;
    } else {
        r = c;
        g = 0;
        b = x;
    }
    m = l - c / 2;
    r = Math.round((r + m) * 255);
    g = Math.round((g + m) * 255);
    b = Math.round((b + m) * 255);
    return {
        r: r,
        g: g,
        b: b
    };
}
 function getNewColor(ratio) {
    var color = hsl2rgb(ratio, 100, 50);
    var hex = figorgbToHex(color.r, color.g, color.b);
    return(hex);
}


Hooks.on("preUpdateActor", (actor, updateData) => {
  const newHP = updateData?.data?.attributes?.hp?.value;
  const maxHP = actor?.data?.data?.attributes?.hp?.max;

  if (!isNaN(newHP) && !isNaN(newHP)) {
    
    var percReal = Math.round((newHP / maxHP) * 100);
    var perc = Math.round((newHP / maxHP) * 120);
    if(percReal <= 50 && percReal > 0){
        var newColor  = getNewColor(perc);
    } else {
        var newColor  = null;
    }  
    
    //console.log(`Hitpoints changed to ${newHP}`);

    if(actor?.parent?.isEmbedded && actor?.parent?.documentName == "Token")
    {
      canvas.scene.updateEmbeddedDocuments(actor.parent.documentName,  [{
        tint: newColor,
        _id: actor.parent.id,
      }]);
    }

    const applicableTokens = canvas.tokens.placeables.filter(
      (token) => token.data.actorId == actor.id && token.data.actorLink
    );

    if(actor.data.actorLink)
    {
      actor.data.token.tint = newColor;
    }

    applicableTokens.forEach((token) => {
      canvas.scene.updateEmbeddedDocuments(Token.embeddedName, [{
        tint: newColor,
        _id: token.data._id,
      }]);
    });
  }
});


function refresh_token_tint(token) {
  const newHP = token.actor.system.attributes.hp.value;
  const maxHP = token.actor.system.attributes.hp.max;

  if (!newHP || !maxHP || isNaN(newHP) || isNaN(newHP))
    return;

  var percReal = Math.round((newHP / maxHP) * 100);
  var perc = Math.round((newHP / maxHP) * 120);
  if(percReal <= 50 && percReal > 0){
      var newColor  = getNewColor(perc);
  } else {
      var newColor  = null;
  }  

  
  canvas.scene.updateEmbeddedDocuments(Token.embeddedName, [{
    tint: newColor,
    _id: token.id,
  }]);  
}

Hooks.on("createToken", refresh_token_tint);
Hooks.on("refreshToken", refresh_token_tint);



Hooks.on("renderActorSheet", (...args) => {	// This is just for debugging, it prevents this sheet's template from being cached.
$('div.monsterblock:not(:has(div.figotooltip))').each(function(){
      $('<div class="figotooltip"></div><div id="biotemp" ></div>')
      .appendTo(this)
      .css({'display': 'none'});
  });
  
  $('.rd__wrp-image a').replaceWith(function() {
   return $('img', this);
  });
  $("#biotemp").html(args[2].actor.data.details.biography.value);
  let avatarimgsrc = $('#biotemp').find('img:first').attr('src');
  if (typeof avatarimgsrc === "undefined") {
  // ...
  } else {
      $('.profile-image').off('click');
      $(".profile-image").click((event) => {
    event.preventDefault();
    new ImagePopout(avatarimgsrc, {
      title: args[2].actor.name,
      shareable: true,
      uuid: args[2].actor.uuid
    }).render(true);
  });
  }
  //$(".spellbook").find(".spell-school").remove();
    $('li.spell').hover(
          function(event){ 
              let spellid = $(this).data('item-id');
              let actordivid = $(this).closest('div.monsterblock').attr("id");
              let tooltipfigo = $(this).closest('div.monsterblock').find('div.figotooltip');
              let position = $(this).position();
              let paleft = event.pageX;
              let patop = event.pageY ;
              let pleft = paleft-400;
              
              if (pleft < 50){
                  pleft = 50;
              }
              if (patop > 500){
                  let whei = $( window ).height();
                  let ptop = whei-patop +30;
                  //let ptop = patop -3;
                  tooltipfigo.css({'bottom': ptop,'top': '', 'left': pleft});
              } else {
                  
                  let ptop = patop +30;
                  tooltipfigo.css({'bottom': '','top': ptop, 'left': pleft});
              }
  
              let actoridarr = actordivid.split("-");
              let actorid = actoridarr[1];
              //console.log('hover spell '+spellid + ' actor '+ actorid);
              const actor = game.actors.get(actorid);
              const spell = actor.items.find(i => i._id ===spellid);
              //console.log(spell);
              let spellname = spell.data.name;
              //console.log('spell '+spellname + ' source '+ spell.data.data.source);
              let figoHtmlspellname = "<div class='fspellname'>" + spellname  +"</div>";
              let figoHtmlsource = '<div class="fsource">' + spell.data.data.source +'</div>';
              let figoHtmldesc = '<div class="fdesc">' + spell.data.data.description.value +'</div>';
              let figoHtmlhr = '<hr class="figohr">';
              let figoHtml = figoHtmlspellname + figoHtmlsource + figoHtmlhr+ figoHtmldesc;
              tooltipfigo.html(figoHtml);
              //
              tooltipfigo.show();
          },
          function(){ 
             let tooltipfigo = $(this).closest('div.monsterblock').find('div.figotooltip');
             tooltipfigo.hide();
             
          }
      );
  });